import cv2
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.svm import SVC
from collections import Counter
from os import listdir
from sklearn.externals import joblib
import pickle

def ls(ruta):
    return listdir(ruta)
def siff():
    for  folders in ls("imagenes"):
	print(folders)
        for files in ls("imagenes" + "/"+ folders):
            print(files)
            img = cv2.imread("imagenes/" + folders + "/" + files) 
	    height = np.size(img, 0)
	    width = np.size(img, 1)
	    print(width)
	    print(height)
            img = cv2.resize(img, (250, 250)) 
            gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
            sift = cv2.xfeatures2d.SIFT_create()
            kp, des = sift.detectAndCompute(gray,None)
            df = pd.DataFrame(des)
            temp = len(files)
            df.to_csv("sifts/" + folders + "/" + files[:temp-3] + "csv", index=False)
            #file= open("Reto3/" + folders + "/"  + files[:temp-3] + "txt", "w")
            #for sift in des:
#siff()
def kmeans():
    cont = 0
    j = 1
    k=50
    dir="sifts"
    kmeansGigante = []
    # for  folders in ls("Reto3"):
    #     dictLabelBloques[folders] = j
    imagenesLengs = []
    for folders in ls(dir):
        files =ls(dir+"/"+folders)
        for i in range(0,len(files)):
            df = pd.read_csv(dir+"/"+folders+"/"+files[i])
            print("Agregando imagen: "+str(dir) + "/" +str(folders) + "/"+ str(files[i]))
            imagenesLengs.append(len(df.values))
	    for values in df.values:
		kmeansGigante.append(values)
    #print(imagenesLengs)
    df = pd.DataFrame(imagenesLengs)
    df.to_csv("labels2.csv", index=False)
    kmeans = KMeans(n_clusters=200, random_state=0)
    kmeans.fit(kmeansGigante)
    joblib.dump(kmeans, 'kmeans2.pkl')
            #if len(df.values) > k:
            #    kmeans = KMeans(n_clusters=k, random_state=0)
            #    labels = kmeans.fit_predict(df.values)
            #    quant = kmeans.cluster_centers_.astype("uint8")[labels]
            #    temporalDictLabel = {}
            #    temporalDict = {}
            #    for i in range(0,len(labels)):
            #        if labels[i] in temporalDictLabel:
            #            temporalDictLabel[labels[i]] +=1
            #        else:
            #            temporalDict[labels[i]] = quant[i]
            #            temporalDictLabel[labels[i]] =1
            #    
            #    
            #    m = []
            #    cont += 1
            #    print("Imagenes analizadas" + str(cont))
            #    for key in temporalDictLabel:
            #        
            #        m2 = []
            #        m2.append(j)
            #        m2.append(temporalDictLabel[key])
            #        for sift in temporalDict[key]:
            #            m2.append(sift)
            #        m.append(m2)
            #    
            #    df = pd.DataFrame(m)
            #    temp = len(siff)
            #    print("Generando "+dir+"/"+str(folders) + str(siff[:temp-3]) + "csv")
            #    print("En la carpeta" + str(folders))
            #    df.to_csv(dir+"/" +folders + siff[:temp-3] + "csv", index=False)
        #j +=1
#kmeans()
def histogramas():
	raiz = "sifts"
	kmeans = joblib.load("kmeans2.pkl")
	matrixFinal = []
	dictLabelsFolder = {}
	labels = []
	i = 1
	for folder in ls(raiz):
		dictLabelsFolder[i] = folder
		for files in ls(raiz+"/"+folder):
			arrayFin = [0]*200
			labels.append(i)
			df = pd.read_csv(raiz+"/"+folder+"/"+files)
			predict = kmeans.predict(df.values)
			a = Counter(predict)
			for keys in a.keys():
				arrayFin[keys] = a[keys]
   			arrayValues = np.array(arrayFin,dtype=float)
			#arrayValues.astype(float)
			maxValue = max(arrayFin)
			matrixFinal.append(arrayValues/maxValue)
			#print(arrayValues/maxValue)
			#input("wait for a key!")
		i+=1
	print(dictLabelsFolder)
	df = pd.DataFrame(matrixFinal)
	dfL = pd.DataFrame(labels)
	df.to_csv("model2.csv", index=False)
	dfL.to_csv("labels2.csv",index=False)
	#print(labels)
##histogramas()
def suport():
    dfX = pd.read_csv("model2.csv")
    dfY = pd.read_csv("labels2.csv")
    print(dfY.values.flatten())
    X_train, X_test, y_train, y_test = train_test_split(dfX.values, dfY.values.flatten(), test_size=0.1, random_state=42)
    print(len(X_train))
    X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.2, random_state=42)
    print(len(X_train))
    #clf = KNeighborsClassifier(n_neighbors=10)
    clf = SVC()
    print(X_train)
    #clf = MLPClassifier(solver='lbfgs', random_state=1)
    clf.fit(X_train, y_train)
    joblib.dump(clf, 'modelBueno2.pkl')
    #clf = joblib.load('model.pkl')
    predictions = clf.predict(X_val)
    print(predictions)
    print(accuracy_score(y_val, predictions))
    print(confusion_matrix(y_val, predictions))
suport()
#sift = cv2.xfeatures2d.SIFT_create()quant   
